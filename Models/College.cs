﻿using System.Text.Json.Serialization;

namespace FirstProject.Models
{
    public class College
    {
        public int CollegeId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        [JsonIgnore]
        public List<Student>? Students { get; set; }
    }

}
