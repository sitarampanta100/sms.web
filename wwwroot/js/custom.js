﻿$(document).ready(function () {
    ShowStudent();
});

function ShowStudent() {
    $.ajax({

        url: '/Ajax/StudentList',
        type: 'Get',
        dataType: 'json',
        contentType: 'application/json;charset=utf-8;',

        success: function (result, statu, xhr) {

            var object = "";
            $.each(result, function (index, item) {

                object += '<tr>';
                object += '<td>' + item.Id + '</td>';
                object += '<td>' + item.Name + '</td>';
                object += '<td>' + item.Age + '</td>';
                object += '<td>' + item.Class + '</td>';
                object += '<td>' + (item.College ? item.College.Name : '') + '</td>';
                object += '</tr>';
            });
            $('#table_data').html(object);
        },

        error: function () {

            alert("Data can't found");

        }




    });


};