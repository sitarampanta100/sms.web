﻿using FirstProject.Data;
using FirstProject.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Identity.Client;
using System.Linq;

namespace FirstProject.Controllers
{

    public class StudentController : Controller
    {
        private readonly StudentDbContext studentDb;
        public StudentController(StudentDbContext context)
        {
            studentDb = context;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public IActionResult GetStudents()
        {
            var students = studentDb.Students.Include(c => c.College).ToList();
            return Json(new { data = students });
        }



        [HttpGet]
        public IActionResult Add()
        {
            ViewBag.collegeList = new SelectList(studentDb.Colleges.ToList(), "CollegeId", "Name");
            return View();


        }

        [HttpPost]
        public IActionResult Add(IEnumerable<Student> Students)
        {
            foreach (var item in Students)
            {
                studentDb.Students.Add(item);
            }

            studentDb.SaveChanges();

            return RedirectToAction(nameof(Index));

        }
        [HttpGet]
        public async Task<IActionResult> Edit()
        {
            var students = await studentDb.Students.ToListAsync();
            var colleges = await studentDb.Colleges.ToListAsync();

            ViewBag.collegeList = colleges.Select(c => new SelectListItem { Value = c.CollegeId.ToString(), Text = c.Name }).ToList();

            return View(students);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(List<Student> students)
        {
            if (ModelState.IsValid)
            {
                foreach (var student in students)
                {
                    studentDb.Update(student);
                }
                await studentDb.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
                var colleges = await studentDb.Colleges.ToListAsync();
                ViewBag.collegeList = colleges.Select(c => new SelectListItem { Value = c.CollegeId.ToString(), Text = c.Name }).ToList();

                 return View(students);
        }

        [HttpGet]
        public IActionResult Delete()
        {
            var std = studentDb.Students.ToList();
            return View(std);
        }

        [HttpPost]
        public ActionResult Delete(int[] studentIds)
        {
            if (studentIds != null && studentIds.Length > 0)
            {
                var studentsToDelete = studentDb.Students.Where(s => studentIds.Contains(s.Id));
                studentDb.Students.RemoveRange(studentsToDelete);
                studentDb.SaveChanges();
            }

            return RedirectToAction("Index");
        }






    }
}

